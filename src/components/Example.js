// // For ES5 builds, import from 'pts/dist/es5'. For ES6 or custom builds, import from 'pts'.
// import {
//   Pt,
//   Group,
//   Line,
//   Create,
//   Sound,
//   Triangle,
//   Const,
//   Geom,
// } from "pts/dist/es5";
// import { PtsCanvas } from "react-pts-canvas";

// /**
//  * Chart example component, which extends PtsCanvas
//  */
// export class ChartExample extends PtsCanvas {
//   _renderChart() {
//     // Given the data, distribute bars across the space's size
//     let w = this.space.size.x / this.props.data.length;
//     let bars = this.props.data.map((d, i) => {
//       return new Group(
//         new Pt(i * w, this.space.size.y),
//         new Pt(i * w, this.space.size.y - d * this.space.size.y - 1)
//       );
//     });

//     // Draw a line controlled by mouse pointer
//     let line = new Group(
//       new Pt(0, this.space.pointer.y),
//       new Pt(this.space.size.x, this.space.pointer.y)
//     );
//     this.form.stroke("#fff", 3).line(line, 10, "circle");

//     // Draw the bars and also check intersection with the pointer's line
//     let intersects = bars.map((b, i) => {
//       this.form.stroke("#123", w - 1).line(bars[i]);
//       return Line.intersectLine2D(b, line);
//     });

//     // Draw intersection points
//     this.form.fillOnly("#f6c").points(intersects, w / 2);
//   }

//   // Override PtsCanvas' animate function
//   animate(time, ftime) {
//     this._renderChart();
//   }

//   // Override PtsCanvas' action function
//   action(type, x, y) {
//     this.space.clear(); // since we're not animating continuously, manually clear canvas and re-render chart
//     this._renderChart();
//   }

//   // Override PtsCanvas' resize function
//   resize(size, evt) {
//     if (this.form.ready) {
//       this.space.clear();
//       this._renderChart();
//     }
//   }
// }

// /**
//  * Chart example component, which extends PtsCanvas
//  */
// export class AnimationExample extends PtsCanvas {
//   constructor() {
//     super();
//     this.noiseGrid = [];
//   }

//   _create() {
//     // Create a line and a grid, and convert them to `Noise` points
//     let gd = Create.gridPts(this.space.innerBound, 20, 20);
//     this.noiseGrid = Create.noisePts(gd, 0.05, 0.1, 20, 20);
//   }

//   componentDidUpdate() {
//     if (this.props.pause) {
//       this.space.pause();
//     } else {
//       this.space.resume();
//     }
//   }

//   // Override PtsCanvas' start function
//   start(space, bound) {
//     this._create();
//   }

//   // Override PtsCanvas' resize function
//   resize() {
//     this._create();
//   }

//   // Override PtsCanvas' animate function
//   animate(time, ftime) {
//     if (!this.noiseGrid) return;

//     // Use pointer position to change speed
//     let speed = this.space.pointer
//       .$subtract(this.space.center)
//       .divide(this.space.center)
//       .abs();

//     // Generate noise in a grid
//     this.noiseGrid.forEach((p) => {
//       p.step(0.01 * (1 - speed.x), 0.01 * (1 - speed.y));
//       this.form
//         .fillOnly("#123")
//         .point(p, Math.abs((p.noise2D() * this.space.size.x) / 18), "circle");
//     });
//   }
// }
// let space;
// export class FloatySpace extends PtsCanvas {
//     canvas = floatySpace => {
//         var colors = [
//           "#FF3F8E", "#04C2C9", "#2E55C1"
//         ];
//         let space = new CanvasSpace("canvas", "#252934" ).display();
//         //space = new CanvasSpace("canvas", "#252934" ).display();
//         var form = new Form( space );
      
//         // Elements
//         var pts = [];
//         var center = space.size.$divide(1.8);
//         var angle = -(window.innerWidth * 0.5);
//         var count = window.innerWidth * 0.05;
//         if (count > 150) count = 150;
//         var line = new Line(0, angle).to(space.size.x, 0);
//         var mouse = center.clone();
      
//         var r = Math.min(space.size.x, space.size.y) * 1;
//         for (var i=0; i<count; i++) {
//           var p = new Vector( Math.random()*r-Math.random()*r, Math.random()*r-Math.random()*r );
//           p.moveBy( center ).rotate2D( i*Math.PI/count, center);
//           p.brightness = 0.1
//           pts.push( p );
//         }
      
//         // Canvas
//         space.add({
//           animate: function(time, fps, context) {
      
//             for (var i=0; i<pts.length; i++) {
//               // rotate the points slowly
//               var pt = pts[i];
      
//               pt.rotate2D( Const.one_degree / 20, center);
//               form.stroke( false ).fill( colors[i % 3] ).point(pt, 1);
      
//               // get line from pt to the mouse line
//               var ln = new Line( pt ).to( line.getPerpendicularFromPoint(pt));
      
//               // opacity of line derived from distance to the line
//               var opacity = Math.min( 0.8, 1 - Math.abs( line.getDistanceFromPoint(pt)) / r);
//               var distFromMouse = Math.abs(ln.getDistanceFromPoint(mouse))
      
//               if (distFromMouse < 50) {
//                 if (pts[i].brightness < 0.3) pts[i].brightness += 0.015
//               } else {
//                 if (pts[i].brightness > 0.1) pts[i].brightness -= 0.01
//               }
      
//               var color = "rgba(255,255,255," + pts[i].brightness +")"
//               form.stroke(color).fill( true ).line(ln);
//             }
//           },
      
//           onMouseAction: function(type, x, y, evt) {
//             if (type=="move") {
//               mouse.set(x,y);
//             }
//           },
      
//           onTouchAction: function(type, x, y, evt) {
//             this.onMouseAction(type, x, y);
//           }
//         });
      
//         space.bindMouse();
//         space.play();
//       }
// }
