import React, { Component } from "react";
import Projects from "./Projects";
import Profiles from "./Profiles";
import Titles from "./Titles";
import Skills from "./Skills";
import About from "./About";
// import { QuickStartCanvas } from "react-pts-canvas";
// import { ChartExample, AnimationExample, FloatySpace } from "./Example";
import { Util, Line } from "pts";

class App extends Component {
  // constructor() {
  //   super();
  //   this.state = { displayBio: false };
  //   this.toggleDisplayBio = this.toggleDisplayBio.bind(this);
  // }
  constructor(props) {
    super(props);

    this.state = {
      variance: 0.2,
      pauseAnimation: false,
    };
  }
  render() {
    return (
      <div>
        {/* <FloatySpace/> */}
        <section className="aboutMe">
          <div className="container h-100">
            <div className="row h-100">
              <div className="col-md-12 align-self-center">
                <h1>
                  HELLO, I'm{" "}
                  <span className="text-primary">Muhammad Mustafa</span>
                </h1>
                <Titles />
                <About />
              </div>
            </div>
          </div>
        </section>

        <div className="container" style={{ paddingBottom: "70px" }}>
          <Skills />
          <hr />
          <Projects />
          <Profiles />
        </div>
      </div>
    );
  }
}

export default App;
