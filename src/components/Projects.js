import React from "react";
import PROJECTS from "../data/projects";

const Project = (props) => {
  // Destructuring
  const { title, image, description, link } = props.project;

  return (
    <div className="col-md-4">
      <div className="card" style={{ minHeight: "370px" }}>
        <div className="card-body">
          <h5 className="card-title">{title}</h5>
          <img src={image} alt="Profile" className="img-fluid" />
          <p className="card-text mt-3">{description}</p>
          <a href={link}>{link}</a>
        </div>
      </div>
    </div>
  );
};

const Projects = () => (
  <div className="mt-5">
    <h2>Highlighted Projects</h2>
    <div className="row mt-4">
      {PROJECTS.map((PROJECTS) => (
        // Returning the function output
        <Project key={PROJECTS.id} project={PROJECTS} />
      ))}
    </div>
  </div>
);

export default Projects;
