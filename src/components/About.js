import React, { Component } from "react";
import { CSSTransition } from "react-transition-group";

class About extends Component {
  state = { displayBio: false };
  toggleDisplayBio = () => {
    this.setState({ displayBio: !this.state.displayBio });
  };
  render() {
    return (
      <div className="mt-4">
        {this.state.displayBio ? (
          // Displaying Bio Data
          <CSSTransition
            in={true}
            appear={true}
            timeout={1000}
            classNames="fade"
          >
            <div>
              <p>I live Karachi, Pakistan</p>
              <p>I think Reactjs is awesome!</p>
              <p>My favourite language is JavaScript!!!!!</p>
              <button
                className="btn btn-primary"
                onClick={this.toggleDisplayBio}
              >
                Show Less
              </button>
            </div>
          </CSSTransition>
        ) : (
          <div>
            <button className="btn btn-primary" onClick={this.toggleDisplayBio}>
              Read More
            </button>
          </div>
        )}
      </div>
    );
  }
}

export default About;
