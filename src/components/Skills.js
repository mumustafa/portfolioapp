import React from "react";
import SKILLS from "./../data/skills";
import classNames from "classnames";

const Skill = (props) => {
  // Destructuring
  const { icon, skill } = props.skills;
  let iconDefault = classNames("icon");
  return (
    <div>
      <i
        className={iconDefault}
        title={skill}
        style={{ backgroundImage: `url(${icon})` }}
      ></i>
      {/* <span>{skill}</span> */}
    </div>
  );
};

const Skills = () => {
  return (
    <div className="my-4">
      <ul className="d-flex justify-content-center imageIconList">
        {SKILLS.map((SKILLS) => (
          <li className="imageIconListItems" key={SKILLS.id}>
            <Skill key={SKILLS.id} skills={SKILLS} />
          </li>
        ))}
      </ul>
    </div>
  );
};

export default Skills;
