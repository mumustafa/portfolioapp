import React from "react";
import PROFILES from "../data/profiles";
import classNames from "classnames";

const Profile = (props) => {
  // Destructuring
  const { profileName, profileLink, profileIcon } = props.profile;
  let btnClass = classNames("fa-2x fa", profileIcon);
  return (
    <div className="col-4 border-1 rounded">
      <a className="d-flex justify-content-center" href={profileLink} target="_blank">
        <i className={btnClass}></i>
        <h3 className="pl-2 align-self-center">{profileName}</h3>
      </a>
    </div>
  );
};

const Profiles = () => {
  return (
    <div className="row mt-4 profile-wrapper">
      {PROFILES.map((PROFILES) => (
        <Profile key={PROFILES.id} profile={PROFILES} />
      ))}
    </div>
  );
};

export default Profiles;
