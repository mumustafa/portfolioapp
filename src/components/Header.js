import React from "react";
import { Link } from "react-router-dom";

const Header = ({ children }) => {
  return (
    <div>
      <nav className="navbar navbar-expand-sm navbar-dark bg-dark navModifier">
        <div className="container text-center collapse navbar-collapse justify-content-md-center">
          <ul className="navbar-nav">
            <li className="nav-item">
              <Link className="nav-link" to="/">
                Home
              </Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link" to="/jokes">
                Jokes
              </Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link" to="/music-master">
                Music Master
              </Link>
            </li>
          </ul>
        </div>
      </nav>
      {children}
    </div>
  );
};

export default Header;
