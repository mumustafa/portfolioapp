import React, { Component } from "react";

const TITLES = ["Front-End", "User Interface", "Web Developer"];

class Titles extends Component {
  // Defining state
  state = { titleIndex: 0, fadeIn: true };

  componentDidMount() {
    this.timeout = setTimeout(() => {
      this.setState({ fadeIn: false });
    }, 2000);
    this.animateTitles();
  }
  componentWillUnmount() {
    clearTimeout(this.timeout);
  }
  animateTitles = () => {
    setInterval(() => {
      const titleIndex = (this.state.titleIndex + 1) % TITLES.length;
      this.setState({ titleIndex, fadeIn: true });
      setTimeout(() => {
        this.setState({ fadeIn: false });
      }, 2000);
    }, 4000);
  };
  render() {
    const { fadeIn, titleIndex } = this.state;
    const title = TITLES[titleIndex];

    return (
      <div>
        <h4 className="h1">
          I am a
          <span className={fadeIn ? "titleFadeIn" : "titleFadeOut"}>
            {" "}
            <span
              className="text-primary text-left d-inline-block"
              style={{ minWidth: "280px" }}
            >
              {title}
            </span>
          </span>
        </h4>
        <em>Please click on "Music Master" in the navigation below</em>
      </div>
    );
  }
}

export default Titles;
