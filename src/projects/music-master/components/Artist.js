import React from "react";

const Artist = ({ artist }) => {
  // Safeguard: If artist is not defined return NULL
  // The component will not return anything
  if (!artist) return null;

  const { images, name, followers, genres } = artist;
  return (
    <div className="mt-4">
      <div className="row">
        <div className="col-3">
          {/* Contains more than 1 images, 
        we are getting 1st images from ARRAY  images[0] && images[0].url*/}
          <img
            className="rounded-circle border img-fluid"
            src={images[0] && images[0].url}
            alt=""
          />
        </div>
        <div className="col-9 align-self-center">
          <div className="text-left">
            <h3 className="text-primary">{name}</h3>
            <p>{followers.total} followers</p>
            {/* Genres will be seprated by comma */}
            <p>{genres.join(", ")}</p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Artist;
