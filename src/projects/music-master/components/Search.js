import React, { Component } from "react";

class Search extends Component {
  state = { artistQuery: "" };
  // Anything typed in search bar
  updateArtistQuery = (event) => {
    // Storing search field value in the state
    this.setState({ artistQuery: event.target.value });
  };
  handleKeyPress = (event) => {
    if (event.key === "Enter") {
      this.searchArtist();
    }
  };
  searchArtist = () => {
    this.props.searchArtist(this.state.artistQuery);
  };
  render() {
    return (
      <div>
        <div className="form-group w-50 mx-auto">
          <div className="input-group mb-3">
            <input
              onKeyPress={this.handleKeyPress}
              onChange={this.updateArtistQuery}
              type="text"
              className="form-control"
              placeholder="Search for your artist"
            />
            <div className="input-group-append">
              <button onClick={this.searchArtist} className="btn btn-primary">
                <i className="icon-search"></i>
                Search
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Search;
