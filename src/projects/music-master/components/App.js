import React, { Component } from "react";
import Artist from "../components/Artist";
import Tracks from "../components/Tracks";
import Search from "./Search";
const API_ADDRESS = "https://spotify-api-wrapper.appspot.com";
class App extends Component {
  // Initializing states
  state = { artist: null, tracks: [] };

  // Will fire on  search button click
  // passing down the call back function
  searchArtist = (artistQuery) => {
    // Fetching data from the API, using template variable
    // Fetch works as a 'Promise'
    fetch(`${API_ADDRESS}/artist/${artistQuery}`)
      // Storing the API result in Json
      .then((response) => response.json())

      .then((json) => {
        if (json.artists.total > 0) {
          const artist = json.artists.items[0];

          this.setState({ artist });
          
          // PROMISES
          fetch(`${API_ADDRESS}/artist/${artist.id}/top-tracks`)
            .then((response) => response.json())
            .then((json) => this.setState({ tracks: json.tracks }))
            .catch((error) => alert(error.message));
        }
      })
      .catch((error) => alert(error.message));
  };

  render() {
    return (
      <div>
        <div className="container py-5">
          <div className="text-center">
            <h1 className="text-uppercase">Music Master</h1>
            {/* Receiving callback function from the parent */}
            <Search searchArtist={this.searchArtist} />
            <Artist artist={this.state.artist} />
            {/* tracks property setting to the current state */}
            <Tracks tracks={this.state.tracks} />
          </div>
        </div>
      </div>
    );
  }
}

export default App;
