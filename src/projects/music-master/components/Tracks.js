import React, { Component } from "react";

class Tracks extends Component {
  // Managing state
  state = { playing: false, audio: null, playingPreviewUrl: null };
  // Double arrow funtion
  // Returning its own callback function
  playAudio = (preveiwUrl) => () => {
    // JavaScript Audio Class
    // Creating new Audio object
    // "audio" is the instance of "Audio JavaScript class"
    const audio = new Audio(preveiwUrl);

    // Checking if playing is false? using the bang operator
    if (!this.state.playing) {
      console.log("play the audio");
      audio.play();
      // Setting the state to true
      this.setState({ playing: true, audio, playingPreviewUrl: preveiwUrl });
    }
    //
    else {
      console.log("Pause the music");
      this.state.audio.pause();

      if (this.state.playingPreviewUrl === preveiwUrl) {
        // Using pause so playing status should be false
        this.setState({ playing: false });
      } else {
        audio.play();
        this.setState({ audio, playingPreviewUrl: preveiwUrl });
      }
    }
  };
  trackIcon = (track) => {
    if (!track.preview_url) {
      return <span>N/A</span>;
    }
    return this.state.playing &&
      this.state.playingPreviewUrl === track.preview_url ? (
      <span>||</span>
    ) : (
      <span>&#9654;</span>
    );
  };
  
  render() {
    // tracks passing down the props
    const { tracks } = this.props;
    // returning JSX
    return (
      <div>
        <div className="row mt-5">
          {/* A map of tracks ARRAY, return each field from the tracks ARRAY through a callback function */}
          {tracks.map((track) => {
            // Destructing the keys from Track Object
            const { id, name, album, preview_url } = track;
            return (
              <div className="col-md-3 mb-4" key={id}>
                <div onClick={this.playAudio(preview_url)} className="card">
                  <img
                    className="card-img-top img-fluid"
                    src={album.images[0].url}
                    alt={album}
                  />
                  <h5 className="card-title trackText">{name}</h5>
                  <p className="trackIcon">{this.trackIcon(track)}</p>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}

export default Tracks;
