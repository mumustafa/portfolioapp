import project1 from "../assets/project1.png";
import project2 from "../assets/project2.png";
import project3 from "../assets/project3.png";

const PROJECTS = [
  {
    id: 1,
    title: "Example React Application",
    description: "A react app that I build.",
    link: "https://bitbucket.org/mumustafa/portfolio-reactjs",
    image: project1
  },
  {
    id: 2,
    title: "Example Angular Application",
    description: "An angular app that I build.",
    link: "https://bitbucket.org/mumustafa/practice-project",
    image: project2
  },
  {
    id: 3,
    title: "Example Angular Application",
    description: "A angular app that I build.",
    link: "https://bitbucket.org/mumustafa/partum",
    image: project3
  }
];

export default PROJECTS