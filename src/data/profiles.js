// import bitbucket from "../assets/bitbucket.png";
// import facebook from "../assets/facebook.png";
// import linkedin from "../assets/linkedin.png";

const PROFILES = [
  {
    id: 1,
    profileName: "Bitbucket",
    profileLink: "https://bitbucket.org/mumustafa/",
    profileIcon: "fa-bitbucket-square"
  },
  {
    id: 2,
    profileName: "Facebook",
    profileLink: "https://facebook.com",
    profileIcon: "fa-facebook-square"
  },
  {
    id: 3,
    profileName: "LinkedIn",
    profileLink: "https://www.linkedin.com/in/muhammad-mustafa-16477a99",
    profileIcon: "fa-linkedin-square"
  }
];

export default PROFILES;
