import skill1 from "../assets/skillicon/sidelogo-816.png";
import skill2 from "../assets/skillicon/sidelogo-818.png";
import skill3 from "../assets/skillicon/sidelogo-819.png";
import skill4 from "../assets/skillicon/sidelogo-820.png";
import skill5 from "../assets/skillicon/sidelogo-821.png";
import skill6 from "../assets/skillicon/sidelogo-822.png";
import skill7 from "../assets/skillicon/sidelogo-823.png";
import skill8 from "../assets/skillicon/sidelogo-824.png";
import skill9 from "../assets/skillicon/sidelogo-825.png";
import skill10 from "../assets/skillicon/sidelogo-826.png";

const SKILLS = [
  {
    id: 1,
    skill: "HTML5",
    icon: skill1,
  },
  {
    id: 2,
    skill: "CSS3",
    icon: skill2,
  },
  {
    id: 3,
    skill: "JavaScript",
    icon: skill3,
  },
  {
    id: 4,
    skill: "ReactJS",
    icon: skill4,
  },
  {
    id: 5,
    skill: "Adobe Photoshop",
    icon: skill5,
  },
  {
    id: 6,
    skill: "WordPress",
    icon: skill6,
  },
  {
    id: 7,
    skill: "Shopify",
    icon: skill7,
  },
  {
    id: 8,
    skill: "Git",
    icon: skill8,
  },
  {
    id: 9,
    skill: "SASS",
    icon: skill9,
  },
  {
    id: 10,
    skill: "Angular",
    icon: skill10,
  },
];

export default SKILLS;
